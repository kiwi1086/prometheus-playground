# prometheus-playground

---

This repository shows up how several services/applications within an internal network could be hidden behind a reverse proxy. To demonstrate this, a Docker Compose configuration is used to hide two applications ([Prometheus](https://prometheus.io/) and [Grafana](https://grafana.com/)) behind a [Nginx](https://www.nginx.com/) reverse proxy.

---

**Table of Contents**

- [Getting Started](#getting-started)


### Getting Started

Start docker-compose service setup

```
$ docker-compose up
```

**Grafana**
[http://localhost:8080/grafana/]()

**Prometheus**
[http://localhost:8080/prometheus]()